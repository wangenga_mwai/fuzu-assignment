import Vue from 'vue'
import App from './App.vue'


require('./assets/css/normalize.css')
require('./assets/scss/main.scss')


new Vue(App).$mount('#app')
